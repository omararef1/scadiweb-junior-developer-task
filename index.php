<?php 

    require_once __DIR__ ."/controller/controller.php";
    
    $controller = new Controller();
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri = explode( '/', $uri );
    //print_r( $uri);
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    //echo $requestMethod;
    $controller->route($uri, $requestMethod);
?>

<!-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>

<body>

    <div class="container mt-3 px-1">
        <div class="row align-items-center">
            <div class="col-4 ">
                <h2 class="display-3"> Product List</h2>
            </div>
            <div class="col-1 offset-5 ">
                <button class="btn  btn-primary">Add</button>
            </div>
            <div class="col-2">
                <button id="delete-product-btn" class="btn btn-outline-danger">Mass Delete</button>
            </div>
            <hr>

        </div>
        //<?php $controller = new Controller();
        //$products = $controller->getAllProducts();
        //echo myList($products);
    //?>
    </div>



</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<script src='script.js'>

</script>

</html> -->