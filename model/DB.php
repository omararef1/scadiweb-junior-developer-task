<?php

require_once __DIR__ . "/config.php";
function model_autoloader($class) {
    include __DIR__ ."/" .$class . '.php';
}
  
spl_autoload_register('model_autoloader');

    class Db {

        protected $conn = null;

        public function __construct () {

            try {
                $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
            
                if ( mysqli_connect_errno()) {
                    throw new Exception("Could not connect to database.");   
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage());   
            }		
        }

        public function selectSql ($type , $category) {
           
            $sqlStatement = "SELECT * FROM products INNER JOIN ".$category." ON products.SKU = ".$category.".SKU";
            $result = $this->conn->query($sqlStatement);
           
            return $result;
        }

        public function deleteSql ($deleteValues ) {
           
            $sqlStatement = "DELETE FROM products WHERE SKU IN".$deleteValues;
            $result = $this->conn->query($sqlStatement);
            return $result;
        }

        public static function getAttributesString ($type) {
            $dict = array(
                "furniture" => "(SKU, height, length, width)", 
                "dvd" => "(SKU, size)", 
                "book" => "(SKU, weight)"
            );
            return $dict[$type];
        }

        public static function getValuesString ($type , $items) {
            
            $dict = array(
                "furniture" => ["height","length","width"], 
                "dvd" => ["size"], 
                "book" => ["weight"]
            );
            $values = $dict[$type];
            $valueString = "('".$items["SKU"]."'" ;
            foreach ($values as $value) {
                $valueString =  $valueString. " , ". $items[$value];
            }
            $valueString = $valueString.")";
           
            return $valueString;
        }

        public function insertSql ($items) {
            //print_r ($items);
            $sqlStatement1 = "INSERT INTO products (SKU, price, name, type) 
                                VALUES ( '".$items["SKU"]."'," .$items["price"].", '".$items["name"]."', '".$items["type"]."' )";

           $attributesString = self::getAttributesString($items["type"]);
          
            $valueString = self::getValuesString($items["type"] , $items);
           $sqlStatement2 = "INSERT INTO ".$items["type"]." ".$attributesString. "VALUES ".$valueString;

           $this->conn->query($sqlStatement1);
           echo $this->conn->error;
           $this->conn->query($sqlStatement2);
        }

        

        public function deleteProducts ($ids) {
            $deleteValues = "( ";
            foreach ($ids as $id) { 
               $deleteValues = $deleteValues."'".$id."', ";
            }
            $deleteValues = rtrim($deleteValues, ", ");
            $deleteValues = $deleteValues." )";
            $this->deleteSql($deleteValues);

        }

        public function getAllProducts () {
            $results = array();
            $result = $this->selectSql("Book","book");
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                   
                    $product = new Book($row["SKU"],$row["name"],$row["price"],$row["weight"]);
                    array_push($results , $product);
                }
            }

            $result = $this->selectSql("Dvd" , "dvd");
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                   
                    $product = new Dvd($row["SKU"],$row["name"],$row["price"],$row["size"]);
                    array_push($results , $product);
                }
            }

            $result = $this->selectSql("Furniture" , "furniture");
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                   
                    $product = new Furniture($row["SKU"],$row["name"],$row["price"],$row["length"],$row["width"],$row["height"]);
                    array_push($results , $product);
                }
            }

            
            return $results;
        }
    }

    


?>