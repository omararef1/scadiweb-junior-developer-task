<?php
    abstract class Product {
        public $SKU ;
        public $name;
        public $price;

            public function __construct ($SKU, $name, $price){
                $this->SKU = $SKU;
                $this->name = $name;
                $this->price = $price;
            }
            public function __toString(){
                $string = "<div class='col'>
                            <div class='card myCard'>
                            <div class='card-header'>
                            <input class='form-check-input' type='checkbox' value='' id='flexCheckDefault' onChange='handleOnChange();'>
                            
                            </label>
                            </div>
                            <div class='p-2'>
                            <h1 class='display-5 text-center'>{$this->SKU} </h1>
                            <h1 class='display-5 text-center'>{$this->name}</h1>
                            <h1 class='display-5 text-center'>{$this->price} $</h1>   
                           ";
                return $string; 
            }
    }
    
    

?>