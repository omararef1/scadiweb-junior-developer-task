<?php

  
spl_autoload_register('model_autoloader');

class Furniture extends Product {
    public $length;
    public $width;
    public $height;

    public function __construct ($SKU, $name, $price , $length , $width , $height){
        parent::__construct($SKU, $name, $price);
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
    }
    public function __toString(){
        $string = parent::__toString();
        $string1 = "<h1 class='display-5 text-center'>Dimension: {$this->length}x{$this->width}x{$this->height}</h1>
                    </div>
                    </div>
                    </div>"
                    ;
        return $string.$string1;  
    }

}
?>