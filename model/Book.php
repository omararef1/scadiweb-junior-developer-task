<?php

  
spl_autoload_register('model_autoloader');

class Book extends Product {
    public $weight ;

    public function __construct ($SKU, $name, $price , $weight){
        parent::__construct($SKU, $name, $price);
        $this->weight = $weight;
    } 
    public function __toString(){
        $string = parent::__toString();
        $string1 = "<h1 class='display-5 text-center'>Weight: {$this->weight} KG</h1>
                    </div>
                    </div>
                    </div>"
                    ;
        return $string.$string1;  
    }

}
?>