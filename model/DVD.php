<?php
function custom_autoloader($class) {
    include  $class . '.php';
  }
  
spl_autoload_register('custom_autoloader');

class Dvd extends Product {
    public $size ;

    public function __construct ($SKU, $name, $price , $size){
        parent::__construct($SKU, $name, $price);
        $this->size = $size;
    } 
    public function __toString(){
        $string = parent::__toString();
        $string1 = "<h1 class='display-5 text-center'>Size: {$this->size} MB</h1>
                    </div>
                    </div>
                    </div>"
                    ;
        return $string.$string1;  
    }

}
?>