<?php
function DB_autoloader($class) {
    include __DIR__ ."/../model/" .$class . '.php';
  }
spl_autoload_register('DB_autoloader');


    class controller {

        private $model; 

        public function __construct () {
            $this->model = new Db();

        }

        function route($uri , $method) {
            switch($method){
                case "GET":
                    $this->getAction($uri);
                    break;
                case "POST":
                    $this->postAction($uri);
                    break;
                default:
                    $this->sendOutput(
                        json_encode(array('error' =>'Method not supported')),
                        array('Content-Type: application/json', 'HTTP/1.1 422 Unprocessable Entity')
                    );
                    break;
            }
        }
        function getAction($uri){
            if ($uri[2]== "products"){
                $data = $this->getAllProducts();
                $this->sendOutput(
                    json_encode($data),
                    array('Content-Type: application/json', 'HTTP/1.1 200 OK')
                );
            }else {
                $this->sendOutput(
                    json_encode(array('error' =>'Method not supported')),
                    array('Content-Type: application/json', 'HTTP/1.1 500 Internal Server Error')
                    );
            }
            
        }
        function postAction($uri){
            $entityBody = json_decode(file_get_contents('php://input'), true);
            
            switch ($uri[2]){
                case "products":
                    $this->insertProduct($entityBody);
                    $data = $this-> getAllProducts();
                    $this->sendOutput(
                        json_encode($data),
                        array('Content-Type: application/json', 'HTTP/1.1 200 OK')
                    );
                    break; 

                case "deleteProduct":
                    $this->deleteProducts($entityBody);
                    $data = $this-> getAllProducts();
                    $this->sendOutput(
                        json_encode($data),
                        array('Content-Type: application/json', 'HTTP/1.1 200 OK')
                    );
                    break;
                
                default:
                    $this->sendOutput(
                        json_encode(array('error' =>'Method not supported')),
                        array('Content-Type: application/json', 'HTTP/1.1 500 Internal Server Error')
                        );
            }
        }


        function getAllProducts () {
            return $this->model->getAllProducts();
        }

        function deleteProducts($ids) {
            return $this->model->deleteProducts($ids);
        }

        function insertProduct($items) {
            return $this->model->insertSql($items);
        }

        


        function sendOutput($data, $httpHeaders=array()){

        header_remove('Set-Cookie');

        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        header('Access-Control-Allow-Headers: token, Content-Type');

            echo $data;
            exit;
        }

        
        
    }
